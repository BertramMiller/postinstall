#!/bin/sh
### OPTIONS AND VARIABLES ###
name="user1"
groupname="user1"
password="default777"
dotfilesrepo="https://gitlab.com/BertramMiller/dotfiles.git" && git ls-remote "$dotfilesrepo"
progsfile="https://gitlab.com/BertramMiller/postinstall/-/raw/master/installs.csv"
aurhelper="yay"
distro="arch"
grepseq="\"^[PGA]*,\""

error() { clear; printf "ERROR:\\n%s\\n" "$1"; exit;}

### FUNCTIONS ###
connectioncheck() { \
	ping -c 3 gnu.org ;}

usercheck() { \
	! (id -u "$name" >/dev/null) 2>&1 || echo "user $name already exists. Overwriting conflicts..."
	sleep 2 ;}

adduserandpass() { \
	echo "Adding a user account: $name"
	useradd -m -g "$groupname" -s /bin/zsh "$name" >/dev/null 2>&1 ||
	usermod -a -G "$groupname" "$name" && mkdir -p /home/"$name" && chown "$name":"$groupname" /home/"$name"
	repodir="/home/$name/.local/src"; mkdir -p "$repodir"; chown -R "$name":"$groupname" $(dirname "$repodir")
	echo "$name:$password" | chpasswd
	unset password ;}

refreshkeys() { \
	echo "Refreshing Arch Keyring..."
	pacman --noconfirm -Sy archlinux-keyring >/dev/null 2>&1 ;}

newperms() { # Set special sudoers settings for install (or after).
	sed -i "/#USER1/d" /etc/sudoers
	echo "$* #USER1" >> /etc/sudoers ;}

installpkg(){ pacman --noconfirm --needed -S "$1" >/dev/null 2>&1 ;}

maininstall() { # Installs all needed programs from main repo.
	echo "Installing \`$1\` ($n of $total). $1 $2"
	installpkg "$1" ;}

gitmakeinstall() {
	progname="$(basename "$1" .git)"
	dir="$repodir/$progname"
	echo "Installing \`$progname\` ($n of $total) via \`git\` and \`make\`. $(basename "$1") $2"
	sudo -u "$name" git clone --depth 1 "$1" "$dir" >/dev/null 2>&1 || { cd "$dir" || return ; sudo -u "$name" git pull --force origin master;}
	cd "$dir" || exit
	make >/dev/null 2>&1
	make install >/dev/null 2>&1
	cd /tmp || return ;}

aurinstall() { \
	echo "Installing \`$1\` ($n of $total) from the AUR. $1 $2"
	echo "$aurinstalled" | grep "^$1$" >/dev/null 2>&1 && return
	sudo -u "$name" $aurhelper -S --noconfirm "$1" >/dev/null 2>&1 ;}

pipinstall() { \
	echo "Installing the Python package \`$1\` ($n of $total). $1 $2"
	command -v pip || installpkg python-pip >/dev/null 2>&1
	yes | pip install "$1" ;}

installationloop() { \
	([ -f "$progsfile" ] && cp "$progsfile" /tmp/installs.csv) || curl -Ls "$progsfile" | sed '/^#/d' | eval grep "$grepseq" > /tmp/installs.csv
	total=$(wc -l < /tmp/installs.csv)
	aurinstalled=$(pacman -Qqm)
	while IFS=, read -r tag program comment; do
		n=$((n+1))
		echo "$comment" | grep "^\".*\"$" >/dev/null 2>&1 && comment="$(echo "$comment" | sed "s/\(^\"\|\"$\)//g")"
		case "$tag" in
			"G") gitmakeinstall "$program" "$comment" ;;
			"A") aurinstall "$program" "$comment" ;;
			"P") pipinstall "$program" "$comment" ;;
			*) maininstall "$program" "$comment" ;;
		esac
	done < /tmp/installs.csv ;}

putgitrepo() {
	echo "downloading and installing dotfiles, old files will be overwritten..."
	[ -z "$3" ] && branch="master" || branch="$repobranch"
	dir=$(mktemp -d)
	[ ! -d "$2" ] && mkdir -p "$2"
	chown -R "$name":"$groupname" "$dir" "$2"
	sudo -u "$name" git clone --recursive -b "$branch" --depth 1 "$1" "$dir" >/dev/null 2>&1
	rsync -aAXv --delete $dir/ $2 ;}

systembeepoff() {
	echo "disabling startup beep sound..."
	rmmod pcspkr
	echo "blacklist pcspkr" > /etc/modprobe.d/nobeep.conf ;}

### THE ACTUAL SCRIPT ###
#welcomemsg || error "User exited"
connectioncheck || error "No internet connection"
installpkg yay || "Are you sure you're running this as the root user and have an internet connection?"
usercheck || error "User exited."
adduserandpass || error "Error adding username and/or password."
refreshkeys || error "Error automatically refreshing Arch keyring. Consider doing so manually."

installpkg curl
installpkg base-devel
installpkg git
installpkg ntp

echo "Synchronizing system time to ensure successful and secure installation of software..."
ntpdate 0.us.pool.ntp.org >/dev/null 2>&1

[ "$distro" = arch ] && { \
	[ -f /etc/sudoers.pacnew ] && cp /etc/sudoers.pacnew /etc/sudoers # Just in case
	newperms "%"$groupname" ALL=(ALL) NOPASSWD: ALL"
	# Use all cores for compilation.
	sed -i "s/-j2/-j$(nproc)/;s/^#MAKEFLAGS/MAKEFLAGS/" /etc/makepkg.conf
	}
# The command that does all the installing. Reads the installs.csv file and
# installs each needed program the way required. Be sure to run this only after
# the user has been created and has priviledges to run sudo without a password
# and all build dependencies are installed.
installationloop
#sudo -u "$name" $aurhelper -S libxft-bgra-git >/dev/null 2>&1
putgitrepo "$dotfilesrepo" "/home/$name" #"$repobranch"
rm -f "/home/$name/README.md" "/home/$name/LICENSE" "/home/$name/FUNDING.yml"
# make git ignore deleted LICENSE & README.md files
git update-index --assume-unchanged "/home/$name/README.md"
git update-index --assume-unchanged "/home/$name/LICENSE"
systembeepoff
# Make zsh the default shell for the user.
chsh -s /bin/zsh $name >/dev/null 2>&1
sudo -u "$name" mkdir -p "/home/$name/.cache/zsh/"
# dbus UUID must be generated for Artix runit.
dbus-uuidgen > /var/lib/dbus/machine-id
# Start/restart PulseAudio.
killall pulseaudio; sudo -u "$name" pulseaudio --start

[ "$distro" = arch ] && newperms "%wheel ALL=(ALL) ALL #USER1
%wheel ALL=(ALL) NOPASSWD: /usr/bin/shutdown,/usr/bin/reboot,/usr/bin/systemctl suspend,/usr/bin/wifi-menu,/usr/bin/mount,/usr/bin/umount,/usr/bin/pacman -Syu,/usr/bin/pacman -Syyu,/usr/bin/packer -Syu,/usr/bin/packer -Syyu,/usr/bin/systemctl restart NetworkManager,/usr/bin/rc-service NetworkManager restart,/usr/bin/pacman -Syyu --noconfirm,/usr/bin/loadkeys,/usr/bin/yay,/usr/bin/pacman -Syyuw --noconfirm"

echo "Installation complete!"
